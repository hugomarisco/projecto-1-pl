%{

#include <string.h>
#include <stdlib.h>
#include "linkedlist.h"

FILE *f_resultados;

char *cleanRaw(char *raw) {
	char *dup = strdup(raw);

	dup = strchr(dup, '>') + 1;

	char *aux = strrchr(dup, '<');

	aux[0] = '\0';

	aux = dup;

	while (aux[0] != '\0') {
		if (aux[0] == '\n') aux[0] = ' ';
		aux++;
	}

	return dup;
}

LL *cidades = NULL;


void adicionarOrganizacao(char *raw) {
	//printf("ORG: %s\n", cleanRaw(raw));
}

void adicionarPessoa(char *raw) {
	//printf("PESSOA: %s\n", cleanRaw(raw));
}

void adicionarPais(char *raw) {
	//printf("PAIS: %s\n", cleanRaw(raw));
}

void adicionarCidade(char *raw) {
	push(&cidades, cleanRaw(raw));
	//printf("CIDADE: %s\n", cleanRaw(raw));
}

%}

%%

\<ENAMEX[ ]TYPE=\"ORGANIZATION\"\>[^<]+\<\/ENAMEX\> { adicionarOrganizacao(yytext); }
\<ENAMEX[ ]TYPE=\"PERSON\"\>[^<]+\<\/ENAMEX\> { adicionarPessoa(yytext); }
\<ENAMEX[ ]TYPE=\"LOCATION\"[ ]SUBTYPE=\"CITY\"\>[^<]+\<\/ENAMEX\> { adicionarCidade(yytext); }
\<ENAMEX[ ]TYPE=\"LOCATION\"[ ]SUBTYPE=\"COUNTRY\"\>[^<]+\<\/ENAMEX\> { adicionarPais(yytext); }
.|\n ;

%%

int main ( int argc, char **argv) {

	f_resultados = fopen("resultados.html", "w");

	fprintf(f_resultados, "<html><head>");
	fprintf(f_resultados, "<meta charset='UTF-8' />");
	fprintf(f_resultados, "<link rel='stylesheet' href='theme.css' />");
	fprintf(f_resultados, "</head><body>");

	yylex();

	print_list(cidades);

	fprintf(f_resultados, "</body><html>");

	fclose(f_resultados);
}
