#ifndef _LL_
#define _LL_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct sLL {
    char *val;
    struct sLL * next;
} LL;

void print_list(LL * head);
void push(LL ** head, char *val);
char *pop(LL ** head);

#endif