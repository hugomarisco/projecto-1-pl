#include "linkedlist.h"

void print_list(LL * head) {
    LL * current = head;

    while (current != NULL) {
        printf("%s\n", current->val);
        current = current->next;
    }
}

void push(LL ** head, char *val) {
    LL * new_node;
    new_node = malloc(sizeof(LL));

    new_node->val = strdup(val);
    new_node->next = *head;
    *head = new_node;
}

char *pop(LL ** head) {
    char *retval = NULL;
    LL * next_node = NULL;

    if (*head == NULL) {
        return NULL;
    }

    next_node = (*head)->next;
    retval = strdup((*head)->val);
    free(*head);
    *head = next_node;

    return retval;
}