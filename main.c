#include <stdio.h>
#include "linkedlist.h"

int main() {
	
	LL * head = NULL;
	push(&head,"Mundo");
	push(&head,"Ola");

	print_list(head);

	char *v = pop(&head);

	printf("%s\n", v);
}